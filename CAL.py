import random
import os.path

score = 0
tried = 0
correctly = 0
Result = ''
def main():
    global score, tried, correctly, Result

    name = input("Please enter your name: ")
    if os.path.isfile(str(name) + ".txt"):
        file = open(str(name) + ".txt", "r")
        print("Hello again " + file.readline())
        score = int(file.readline())
        tried = int(file.readline())
        correctly = int(file.readline())
    else:
        print("Hello, " + name + "!")
        print()
    while True:
        ask()
        print()
        if Result == '':
            print("Goodbye " + name)
            file = open(str(name) + ".txt", "w")
            file.write(name + "\n")
            file.write(str(score) + "\n")
            file.write(str(tried) + "\n")
            file.write(str(correctly) + "\n")
            break
        if Result == "score":
            print("Your score: " + str(score))
            print("Problems tried: " + str(tried))
            print("Problems answered correctly: " + str(correctly))
            if tried > 0:
                print("Percentage: " + str((correctly/tried) * 100) + "%")
            else:
                print("Percentage: 0%")
            print()


def CompareR():
    if Result == '' or Result == "score":
        return True
    else:
        return False

def feedback(Result, Compare):
    global score, correctly
    if str(Result) == str(Compare):
        print("Yes, you got it!")
        score = score + 2
        correctly = correctly + 1
    else:
        print("Nope, is " + str(Compare))
        score = score - 1
    print("Your current score is: " + str(score))

def ask():
    global tried, Result

    a = random.randint(0, 10)
    b = random.randint(0, 10)
    operation = random.randint(1, 3)

    if operation == 1:
        Result = input("What is: " + str(a) + " + " + str(b) + " ? ")
        if CompareR() is True:
            return
        Compare = a + b
        feedback(Result, Compare)
    if operation == 2:
        Result = input("What is: " + str(a) + " - " + str(b) + " ? ")
        if CompareR() is True:
            return
        Compare = a - b
        feedback(Result, Compare)
    if operation == 3:
        Result = input("What is: " + str(a) + " * " + str(b) + " ? ")
        if CompareR() is True:
            return
        Compare = a * b
        feedback(Result, Compare)
    tried = tried + 1
    return

main()